import React from "react";

export const Header = ({ levels, onPress }) => {
  return (
    <div className="header-container">
      <div style={{ display: "flex" }}>
        {levels.map((item, i) => (
          <React.Fragment key={i}>
            <button className="header-button" onClick={() => onPress(i)}>
              <p className="bold">{item}</p>
            </button>
            {i < levels.length - 1 && <p className="bold">/</p>}
          </React.Fragment>
        ))}
      </div>
      <p className="bold">Budgeted</p>
    </div>
  );
};
