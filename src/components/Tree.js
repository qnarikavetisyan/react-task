import React, { useState } from "react";
import { AiOutlineArrowRight } from "react-icons/ai";

export const Tree = ({ data, level }) => {
  return (
    <ul className="tree">
      {data.map((item, i) => {
        return (
          <TreeNode
            key={i}
            node={item}
            visible={level >= +item.level}
            level={level}
          />
        );
      })}
    </ul>
  );
};

const TreeNode = ({ node, visible, level }) => {
  const [isVisible, setIsVisible] = useState(visible);
  return (
    <li>
      <div
        className="node"
        onClick={() => {
          setIsVisible((prev) => !prev);
        }}
      >
        {node.childNodes.length ? (
          <div className={`toggler ${isVisible || visible ? "active" : ""}`}>
            <AiOutlineArrowRight />
          </div>
        ) : null}

        <div className="node-container">
          <p> {node.title}</p>
          <p> {node.amount}</p>
        </div>
      </div>

      {node.childNodes.length && (isVisible || visible) ? (
        <ul>
          <Tree data={node.childNodes} level={level} />
        </ul>
      ) : null}
    </li>
  );
};
