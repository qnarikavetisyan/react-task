export const LEVELS = [
  "department_nm",
  "group_nm",
  "category_nm",
  "sub_cat_nm",
];

export const LEVEL_NAMES = ["Department", "Group", "Category", "Sub-Category"];

export const AMOUNTS = [
  "current_department_am",
  "current_group_am",
  "current_cat_am",
  "current_sub_cat_am",
];
