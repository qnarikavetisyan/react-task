import React, { useEffect, useState } from "react";

import "./App.css";
import { Header, Tree } from "./components";

import { LEVELS, LEVEL_NAMES } from "./constants";
import { formatJSON } from "./helpers";
import { FetchAPI } from "./services";

function App() {
  const [formatData, setFormatData] = useState([]);
  const [toggle, setToggle] = useState(-1);

  useEffect(() => {
    const api = new FetchAPI();
    const getData = async () => {
      try {
        const response = await api.getData();
        const data = await response.json();
        const treeData = formatJSON(data.data, LEVELS);
        setFormatData(treeData);
      } catch (error) {
        console.log(error);
      }
    };
    getData();
  }, []);

  return (
    <>
      <Header
        levels={LEVEL_NAMES}
        onPress={(level) => {
          setToggle((prev) => (prev === level ? -1 : level));
        }}
      />
      <Tree data={formatData} level={toggle} />
    </>
  );
}

export default App;
