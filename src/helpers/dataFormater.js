import { AMOUNTS } from "../constants";

export function formatJSON(data, levels) {
  const dataFormat = formatData(data, levels, 0, AMOUNTS);
  const result = formatArray(dataFormat);
  return result;
}

function formatArray(items) {
  const arr = [];
  for (let i in items) {
    const [title, amount, index] = i.split("--");
    arr.push({
      title,
      amount,
      level: index,
      childNodes: formatArray(items[i]),
    });
  }
  return arr;
}

function formatData(data, level, index, amounts) {
  const section = level[index];
  const current_amount = amounts[index];
  if (!section) return [];

  const child = {};
  data.forEach((item) => {
    const newArr = `${item[section]} -- ${item[current_amount]} -- ${index}`;
    if (!child[newArr]) child[newArr] = [];
    child[newArr].push(item);
  });
  index++;
  for (let item in child) {
    child[item] = formatData(child[item], level, index, amounts);
  }
  return child;
}
