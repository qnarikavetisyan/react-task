export class FetchAPI {
  async getData() {
    try {
      const data = await fetch("data.json", {
        method: "GET",
      });
      return data;
    } catch (error) {
      console.log(error);
    }
  }
}
